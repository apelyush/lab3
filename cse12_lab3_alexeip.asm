# variable triangles
.text
.globl  main
main:
	li $t4, 1 #the number to print
	li $t5, 1 #number of stars to print
	li $v0, 4
    la $a0, prompt  # print prompt
    syscall
inputloop: #makes sure the input is a valid value
    li $v0, 5
    syscall
    bgt $v0, 0, setup
    li $v0, 4
    la $a0, badinput
    syscall
    j inputloop
setup: #intializes variables for the triangle
	la $t1, ($v0) #sets t1 to the input
    la $t6, ($t1) #will be used later for the 1 star case
    add $t1, $t1, 1
lineloop:
	sub $t1, $t1, 1
	beq $t1, 0, end
	sub $t3, $t1, 1 #t3 is how many tabs there should be
	li $v0, 4
tabloop: #inputs the correct number of tabs before the first number
	beq $t3, 0, number1
	la $a0, tab
	sub $t3, $t3, 1
	syscall
	j tabloop
number1: #writes the first number
	li $v0, 1
	la $a0, ($t4)
	syscall
	addi $t4, $t4, 1
	beq $t4, 2, firstline # if we're printing the first number, skip the next loops since we only print 1
	la $t0, ($t5) #t0 is the temporary counter for how many stars there should be
	addi $t5, $t5, 2 #t5 is how many stars there'll be next iteration
stars: #types out each star
	beq $t0, 0, number2
	la $a0, addstar
	li $v0, 4
	syscall
	sub $t0, $t0, 1
	j stars
number2: #types the second number
	la $a0, tab
	syscall
	li $v0, 1
	la $a0, ($t4)
	syscall
	add $t4, $t4, 1
	li $v0, 4
	la $a0, newline
	syscall
	j lineloop
firstline: #handles the first line case
	li $v0, 4
	la $a0, newline
	syscall
	beq $t6, 1, onestar
	j lineloop
onestar: #handles the case in which the user input 1
	la $a0, star
	syscall
end: #end program
	la $a0, newline
	syscall
	li $v0, 10
	syscall
.data
prompt: .asciiz "Enter the height of the pattern (must be greater than 0):	"
star: .asciiz "*"
addstar: .asciiz "	*"
tab: .asciiz "	"
newline: .asciiz "\n"
badinput: .asciiz "Invalid Entry!\nEnter the height of the pattern (must be greater than 0):	"
