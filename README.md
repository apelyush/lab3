# Lab3

- Made by Alexei Pelyushenko
- 2020, Fall Quarter
- Lab3 : Making a triangle out of stars in MIPS
- To run the code, load the file into MARS, click the Assemble Code button in the top right, and press run.
    - You have to do this every time you want to run the code.

This project had us create a triangle out of asterixis in MARS, a program that allows us to code in the low level language MIPS.
It was quite difficult to begin to learn at first. The more and more I used the program however, the more intuitive it became to
code in. It's actually quite simple. The hard part is being efficient. You can't quite put together commands. Each command has to
be individual.

The formatting looks weird in github. It will fix itself when opened in MARS.

- li : loads a value into a register
- la : loads a value from a register into another register
- beq : branch to segment
- j : jump to segment

- $t# : typical register, holds a value
- $a# : print register, holds a value to be printed by v0
- $v0 : syscall register, depending on the value you set it to, it changes what syscall does
    - 1 : print integer
    - 4 : print string
    - 5 : read input
    - 10: quit program
    - 11: print character (this one isn't really that useful compared to 4. Functionally, 4 will do the same thing for 1 character.)